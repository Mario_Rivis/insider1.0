package org.dxworks.dxplatform.plugins.insider.constants;

public interface InsiderConstants {

    String PROJECT_ID = "projectID";
    String ROOT_FOLDER = "rootFolder";

    String CONFIGURATION_FOLDER = "config";
    String RESULTS_FOLDER = "results";

    String CONFIGURATION_FILE = "insider-conf.properties";
    String STARS = "*******************************************************************************";
}
